import os
import urllib
from time import sleep
import json
from blockchain.blockchain import Blockchain
from flask import Flask, jsonify

blockchain = Blockchain()
app = Flask(__name__)


@app.route('/mine_block', methods=['GET'])
def mine_block():
    previous_block = blockchain.get_previous_block()
    previous_proof = previous_block['proof']
    proof = blockchain.proof_of_work(previous_proof)
    previous_hash = blockchain.hash(previous_block)
    block = blockchain.create_block(proof, previous_hash)
    response = block
    response['message'] = 'Congratulations, you mined a block!'
    return json.dumps(response)


@app.route('/blockchain', methods=['GET'])
def get_chain():
    blockchain_chain = blockchain.chain
    response = {
        'chain': blockchain_chain,
        'length': len(blockchain_chain)
    }
    return json.dumps(response)


@app.route('/is_valid', methods=['GET'])
def is_valid():
    if blockchain.is_chain_valid(blockchain.chain):
        response = {'valid': True, 'message': 'Successful, the blockchain is valid!'}
    else:
        response = {'valid': False, 'message': 'Error, the blockchain is invalid!'}
    return json.dumps(response)


# external access to decentralize it
app.run(host='0.0.0.0', port=5000)